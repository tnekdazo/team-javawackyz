package com.bcexpress.controller;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bcexpress.dto.CustomerVO;
import com.bcexpress.entity.Customer;
import com.bcexpress.facade.CustomerFacade;
/**
 * This controller handles the request and response of the service.
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
@Controller
public class CustomerController {
	@Autowired
	private CustomerFacade customerFacade;
	
	/**
	 * This is the mapping for adding customer.
	 *
	 * @param customer
	 * @return HttpStatus.BAD_REQUEST or HttpStatus.CREATED
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> saveCustomer(
			@Valid @RequestBody CustomerVO customer) {
		ErrorDetails errorDetails = new ErrorDetails();
		if (customer.getFirstName().charAt(0) == ' ') {
			errorDetails.setMessage("There is an error in your firstName");
			errorDetails.setDescription("firstName should not be a space");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getFirstName().matches("[!@#$%^&*]+")) {
			errorDetails.setMessage("There is an error in your firstName");
			errorDetails
					.setDescription("firstName should not be special characters");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getFirstName().matches(".*\\d+.*")) {
			errorDetails.setMessage("There is an error in your firstName");
			errorDetails.setDescription("firstName should not contain numbers");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getLastName().charAt(0) == ' ') {
			errorDetails.setMessage("There is an error in your lastName");
			errorDetails.setDescription("lastName should not be a space");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getLastName().matches("[!@#$%^&*]+")) {
			errorDetails.setMessage("There is an error in your lastName");
			errorDetails
					.setDescription("lastName should not be special characters");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getLastName().matches(".*\\d+.*")) {
			errorDetails.setMessage("There is an error in your lastName");
			errorDetails.setDescription("lastName should not contain numbers");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getAddress().charAt(0) == ' ') {
			errorDetails.setMessage("There is an error in your address");
			errorDetails.setDescription("address should not be a space");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getGender().charAt(0) == ' ') {
			errorDetails.setMessage("There is an error in your gender");
			errorDetails.setDescription("gender should not be a space");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getGender().matches("[!@#$%^&*]+")) {
			errorDetails.setMessage("There is an error in your gender");
			errorDetails
					.setDescription("gender should not be special characters");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getAge().charAt(0) == ' ') {
			errorDetails.setMessage("There is an error in your age");
			errorDetails.setDescription("age should not be a space");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (customer.getAge().matches("[!@#$%^&*]+")) {
			errorDetails.setMessage("There is an error in your age");
			errorDetails.setDescription("age should not be special characters");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isAlpha(customer.getAge())) {
			errorDetails.setMessage("There is an error in your age");
			errorDetails.setDescription("age should not be characters");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if(Integer.parseInt(customer.getAge()) < 18){
			errorDetails.setMessage("There is an error in your age");
			errorDetails.setDescription("age should not be less than 18");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else if(Integer.parseInt(customer.getAge()) > 120){
			errorDetails.setMessage("There is an error in your age");
			errorDetails.setDescription("age should not be more than 120");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else {
			Customer savedCustomer = customerFacade.saveCustomer(customer);
			return new ResponseEntity<>(HttpStatus.CREATED);
		}
	}
	/**
	 * This is the mapping for getting the information of the customer.
	 * @param id
	 * @return HttpStatus.BAD_REQUEST or HttpStatus.OK
	 */
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getCustomer(@PathVariable String id) {
		ErrorDetails errorDetails = new ErrorDetails();
		if (!id.matches("[0-9]+")) {
			errorDetails.setMessage("There is an error in you ID");
			errorDetails
					.setDescription("ID should not accept characters nor special characters.");
			return new ResponseEntity<ErrorDetails>(errorDetails,
					HttpStatus.BAD_REQUEST);
		} else {
			try {
				Customer customer = customerFacade.getCustomer(id);
				System.out.println("Name returned is: "
						+ customer.getFirstName());
				return new ResponseEntity<Customer>(customer, HttpStatus.OK);
			} catch (NullPointerException e) {
				errorDetails.setMessage("There is an error in you ID");
				errorDetails.setDescription("ID does not exist in DB");
				return new ResponseEntity<ErrorDetails>(errorDetails,
						HttpStatus.BAD_REQUEST);
			}
		}
	}
	/**
	 * This is the mapping for null and empty URI parameter
	 * @return HttpStatus.BAD_REQUEST
	 */
	@RequestMapping(value = "/get/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ErrorDetails> nullID() {
		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setMessage("There is an error in you ID");
		errorDetails.setDescription("ID should not be null");
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
}
