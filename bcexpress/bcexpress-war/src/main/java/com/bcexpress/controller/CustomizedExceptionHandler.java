package com.bcexpress.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
/**
 * This is custom exception handler for @Valid annotation from CustomerController
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
@ControllerAdvice
@Controller
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler {
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails();
		BindingResult bindingResult = ex.getBindingResult();
		FieldError field = bindingResult.getFieldError();
		 errorDetails.setMessage("There is an error in your " +
		 field.getField());
		 errorDetails.setDescription(field.getField() + " " +
		 field.getDefaultMessage());
		return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
/*		ArrayList<CustomerExceptionDetails> exceptionDetails = new ArrayList<CustomerExceptionDetails>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			// Place errors into CustomerExceptionDetails
			exceptionDetails.add(new CustomerExceptionDetails(error.getField(),
					error.getDefaultMessage()));
		}*/
	}
}
