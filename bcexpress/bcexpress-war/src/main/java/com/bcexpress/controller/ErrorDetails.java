package com.bcexpress.controller;
/**
 * This class contains error details for custom error
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
public class ErrorDetails {
	private String message;
	private String description;

	public ErrorDetails() {

	}

	public ErrorDetails(String message, String description) {
		super();
		this.message = message;
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
