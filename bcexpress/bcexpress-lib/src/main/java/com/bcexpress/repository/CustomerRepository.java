package com.bcexpress.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcexpress.entity.Customer;
/**
 * This is the custom repository that extends JpaRepository
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
	/**
	 * This method is used for saving customer information to database
	 * @param customer
	 * @return Customer
	 */
	public Customer save(Customer customer);
	/**
	 * This is used for retrieving customer information from database
	 * @param id
	 * @return Customer
	 */
	public Customer findById(int id);
}
