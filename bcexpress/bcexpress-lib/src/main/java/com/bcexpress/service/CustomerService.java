package com.bcexpress.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bcexpress.dto.CustomerVO;
import com.bcexpress.entity.Customer;
import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.repository.CustomerRepository;
/**
 * This implements CustomerFacade and this is used for saving a
 * and retrieving information of the Customer
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
@Service
public class CustomerService implements CustomerFacade{
	
	@Autowired
	private CustomerRepository customerRepository;
	
	/**
	 * This method transfers data from CustomerVO customer to Customer myCustomer.
	 * This overrides saveCustomer method from CustomerFacade and saves customer info to database.
	 * @param CustomerVO customer
	 * @return Customer
	 */
	@Override
	@Transactional
	public Customer saveCustomer(CustomerVO customer) {
		System.out.println("I'm at the CustomerService saveCustomer");
		Customer myCustomer = new Customer();
		
		myCustomer.setFirstName(customer.getFirstName());
		System.out.println("Customer First Name: " + myCustomer.getFirstName());
		myCustomer.setLastName(customer.getLastName());
		myCustomer.setAge(Integer.parseInt(customer.getAge()));
		myCustomer.setAddress(customer.getAddress());
		myCustomer.setGender(customer.getGender());
		
		return customerRepository.save(myCustomer);
	}
	/**
	 * This method overrides getCustomer method from CustomerFacade 
	 * and retrieve customer info from database.
	 * @param String id
	 * @return Customer
	 */
	@Override
	@Transactional
	public Customer getCustomer(String id) {
		System.out.println("I'm at the CustomerService getCustomer");
		return customerRepository.findById(Integer.parseInt(id));	
	}
}