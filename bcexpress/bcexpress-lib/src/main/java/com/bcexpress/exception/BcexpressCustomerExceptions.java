package com.bcexpress.exception;

import com.bcexpress.dto.CustomerVO;


/***
 * 
 * @author joshua.y.m.requioma
 *  
 */
public class BcexpressCustomerExceptions extends RuntimeException{
	// Error messages for first name
	private static final String FIRST_NAME_EMPTY = "First name should not be empty!";
	private static final String FIRST_NAME_NULL = "First name should not be null!";
	private static final String FIRST_NAME_NUMERIC = "First name should not be in numbers!";
	private static final String FIRST_NAME_HAS_NUMBERS = "First name should not contain any numbers!";
	private static final String FIRST_NAME_HAS_SPECIAL_CHARACTERS = "First name should not contain any special characters!";
	private static final String FIRST_NAME_MAX_LENGTH = "First name should not exceed 20 characters!";
	
	// Error messages for last name
	private static final String LAST_NAME_EMPTY = "Last name should not be empty!";
	private static final String LAST_NAME_NULL = "Last name should not be null!";
	private static final String LAST_NAME_NUMERIC = "Last name should not be in numbers!";
	private static final String LAST_NAME_HAS_NUMBERS = "Last name should not contain any numbers!";
	private static final String LAST_NAME_HAS_SPECIAL_CHARACTERS = "Last name should not contain any special characters!";
	private static final String LAST_NAME_MAX_LENGTH = "Last name should not exceed 20 characters!";
	
	// Error message for address
	private static final String ADDRESS_EMPTY = "Address should not be empty!";
	private static final String ADDRESS_NULL = "Address should not be null!";
	private static final String ADDRESS_NUMERIC = "Address should not be in numbers!";
	private static final String ADDRESS_HAS_NUMBERS = "Address should not contain any numbers!";
	private static final String ADDRESS_HAS_SPECIAL_CHARACTERS = "Address should not contain any special characters!";
	private static final String ADDRESS_MAX_LENGTH = "Address should not exceed 20 characters!";
	
	public BcexpressCustomerExceptions(String errorMessage, Throwable err){
		super(errorMessage, err);
	}
	
}
