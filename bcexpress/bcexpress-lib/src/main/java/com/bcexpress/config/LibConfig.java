package com.bcexpress.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 * This class contains configuration for database connection.
 * @author john.k.c.virtudazo
 *	@February 4, 2019
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.bcexpress.repository")
@ComponentScan({"com.bcexpress.entity","com.bcexpress.service"})
public class LibConfig {
	
	private static final String PROPERTY_NAME_DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String PROPERTY_NAME_DATABASE_PASSWORD = "abcd1234";
	private static final String PROPERTY_NAME_DATABASE_URL = "jdbc:mysql://localhost:3306/customer_db";
	private static final String PROPERTY_NAME_DATABASE_USERNAME = "root";

	
	// Datasource/JPA configuration

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(mySQLDataSource());
		em.setPackagesToScan("com.bcexpress.entity");
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}

	@Bean
	public DataSource mySQLDataSource() {
		DriverManagerDataSource mySQLDataSource = new DriverManagerDataSource();
		mySQLDataSource.setDriverClassName(PROPERTY_NAME_DATABASE_DRIVER);
		mySQLDataSource.setUsername(PROPERTY_NAME_DATABASE_USERNAME);
		mySQLDataSource.setPassword(PROPERTY_NAME_DATABASE_PASSWORD);
		mySQLDataSource.setUrl(PROPERTY_NAME_DATABASE_URL);
		return mySQLDataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "validate"); //depending on the property set here; it will create or drop a table
		properties.setProperty("hibernate.dialect",
				"org.hibernate.dialect.MySQL5Dialect");

		return properties;
	}

}
