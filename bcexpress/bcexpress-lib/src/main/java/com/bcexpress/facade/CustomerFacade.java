package com.bcexpress.facade;

import org.springframework.stereotype.Service;

import com.bcexpress.dto.CustomerVO;
import com.bcexpress.entity.Customer;
/**
 * This interface will be implemented by CustomerService.
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
@Service
public interface CustomerFacade {
	/**
	 * This method is used for saving customer info.
	 * @param CustomerVO customer
	 * @return Customer
	 */
	public Customer saveCustomer(CustomerVO customer);
	/**
	 * This method is used for retrieving customer info.
	 * @param String id
	 * @return Customer
	 */
	public Customer getCustomer(String id);
}
