package com.bcexpress.dto;

import javax.validation.Constraint;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * This is the data transfer object for Customer entity
 * @author john.k.c.virtudazo
 *	@date February 4, 2019
 */
public class CustomerVO {
	@NotNull(message = "should not be null.")
	@NotEmpty(message = "should not be an empty string.")
	@Size(max = 50, message = "should not exceed 50 characters")
	private String firstName;
	@NotNull(message = "should not be null.")
	@NotEmpty(message = "should not be an empty string.")
	@Size(max = 50, message = "should not exceed 50 characters")
	private String lastName;
	@NotNull(message = "should not be null.")
	@NotEmpty(message = "should not be an empty string.")
	@Size(max = 50, message = "should not exceed 50 characters")
	private String address;
	@NotNull(message = "should not be null.")
	@NotEmpty(message = "should not be an empty string.")
	@Pattern(regexp = "[MF]", message = "should be 'M' or 'F' only")
	private String gender;
//	@Min(value = 18, message = "should not be less than 18")
//	@Max(value = 120, message = "should not be more than 120")
	//@Digits(integer = 3, fraction = 0, message = "should not contain only numbers")
	@NotEmpty(message = "should not be empty")
	@NotNull(message = "should not be null")
//	@Pattern(regexp = "^[0-9]", message = "should only be a number")
	private String age;

	// Default Constructor
	public CustomerVO() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	/*@Pattern.List({
	//@Pattern(regexp = "(?=.*[$&+,:;=\\\\?@#|/'<>.^*()%!-])", message="should not be special characters."),
	@Pattern(regexp = "(?=.*[0-9])", message = "should be characters."),
	@Pattern(regexp = "(?=.*[a-zA-Z0-9])", message = "should not be alphanumeric"),
	@Pattern(regexp = "(?=.*[!@#$%^&*])", message = "should not be special characters.")
		//@Pattern(regexp = "^$", message="should not be an empty string."),
		//@Pattern(regexp = "^[!@#$%&*()_+=|<>?{}\\[\\]~-]", message="should not be special characters."),
		//@Pattern(regexp = "[\\t\\n\\x0b\\r\\f]", message = "should not be a space"),,
		//@Pattern(regexp = "^[a-zA-Z0-9]*", message = "should not contain special characters, numbers and white space")
	})*/
	//@Pattern(regexp = "/^\\s", message = "should not be a space")
	//@Pattern(regexp = "[\\t\\n\\x0b\\r\\f]", message = "should not be a space")
//	@NotNull(message = "should not be null.")
/*	@NotEmpty(message = "should not be an empty string or null or whitespace.")
	@Size(max = 50, message = "should not exceed 50 characters")
	@Pattern.List({
		@Pattern(regexp = "(?=.*[0-9])", message = "should not have special characters and numbers")
	})*/
}
